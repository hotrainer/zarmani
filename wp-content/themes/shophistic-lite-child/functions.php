<?php
function name_enqueue_styles() {
	wp_enqueue_style( 'name-parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'name-style', get_stylesheet_uri(), array( 'name-parent-style' ) );
}
//add_action( 'wp_enqueue_scripts', 'name_enqueue_styles' );